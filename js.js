/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var clicked = false;
var checked = false;



$(document).ready(function () {
    document.getElementById("longhair").addEventListener("change", function () {
        checkLongHair(event);
    });
    document.getElementById("frown").addEventListener("change", function () {
        changeExpression(event);
    });
    var tops = document.getElementsByClassName("clicktop");
    for (i = 0; i < tops.length; i++) {
        tops[i].addEventListener("click", clickTop);
        tops[i].id = i + 1;
    }
    var bottoms = document.getElementsByClassName("clickbottom");
    for (i = 0; i < bottoms.length; i++) {
        bottoms[i].addEventListener("click", clickBottom);
        bottoms[i].id = i + 1;
    }
    var shoes = document.getElementsByClassName("clickshoes");
    for (i = 0; i < shoes.length; i++) {
        shoes[i].addEventListener("click", clickShoes);
        shoes[i].id = i + 1;
    }
    
    var necklace = document.getElementsByClassName("clicknecklace");
    for (i = 0; i < necklace.length; i++) {
        necklace[i].addEventListener("click", clickNecklace);
        necklace[i].id = i + 1;
    }
    
     var none = document.getElementsByClassName("clicknone");
    for (i = 0; i < none.length; i++) {
        none[i].addEventListener("click", clickNone);
        none[i].id = i + 1;
    }
});

function checkLongHair(event) {
    if (checked === false) {
        $("#hair").attr('src', "HannersLongHair.png");
        checked = true;
    }
    else {
        $("#hair").attr('src', "");
        checked = false;
    }
}

function changeExpression(event) {
    if (clicked === false) {
        $("#hannelore").attr('src', "HanneloreBase2.png");
        clicked = true;
    }
    else {
        $("#hannelore").attr('src', "HanneloreBase1.png");
        clicked = false;
    }
}

function clickShoes(event) {
    var id = event.target.id;
    if (id < 10) {
        id = "0" + id;
    }
    var img = "shoes" + id + ".png";
    $("#shoes").attr('src', img);
}

function clickNone(event){
    $("#necklace").attr('src', "");
}
function clickNecklace(event) {
    var id = event.target.id;
    if (id < 10) {
        id = "0" + id;
    }
    var img = "necklace" + id + ".png";
    $("#necklace").attr('src', img);
}

function clickTop(event) {
    var id = event.target.id;
    if (id < 10) {
        id = "0" + id;
    }
    var img = "tops/tops" + id + ".png";
    $("#top").attr('src', img);
}

function clickBottom(event) {
    var id = event.target.id;
    if (id < 10) {
        id = "0" + id;
    }
    var img = "bottoms/bottoms" + id + ".png";
    $("#bottom").attr('src', img);
}

function makeArray(article) {
    var images = new Array();
    for (i = 1; i < 4; i++) {
        images[i] = new Image();
        var strI = i + "";
        if (i < 10) {
            strI = "0" + i + "";
        }
        images[i].src = article + "/" + article + strI + ".png";
        images[i].id = i;
    }
    return images;
}